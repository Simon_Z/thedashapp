import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
import pandas as pd
from dash.dependencies import Input, Output, State

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


Orders = pd.read_excel("suivi.xlsx", sheet_name="données")

Colors = { "text" : "#990033",
          "plot_color" : "#990033",
          "paper_color" : "#990033",
          "backgroundcolor" : "rgb(255, 0, 0)"}


app.layout = html.Div([
   
    html.H1(children = 'DaaS V6',
            style= {
                'textAlign' : 'Center',
                'color' : Colors["text"]
                }
            ),
    
    html.Br(),
    html.Br(), 
    html.Br(),
    html.Br(),
   
   
    
    html.Div(children = 'Devops As a Service',
             style= {
                'textAlign' : 'Center',
                'color' : Colors["text"]
                }
             ),
    
    html.Br(),
    html.Br(),

    
    html.Div([
        html.Div([
            dcc.Graph(
                id='scatter two',
                figure={
                    'data': [
                        go.Scatter(
                            x = Orders.lacolonne1,
                            y = Orders.lacolonne2,
                            mode = "markers"
                            )
                        ],
                    'layout': go.Layout(
                        title = "les sales et les profits",
                        xaxis = {"title" : "sales"},
                        yaxis = {"title" : "Profit"}
                        )
                }
            )
        ], className="six columns"),

        html.Div([
            dcc.Graph(
                id='scatter three',
                figure={
                    'data': [
                        go.Scatter(
                            x = Orders.lacolonne1,
                            y = Orders.lacolonne2,
                            mode = "markers"
                            )
                        ],
                    'layout': go.Layout(
                        title = "les sales et les profits",
                        xaxis = {"title" : "sales"},
                        yaxis = {"title" : "Profit"}
                        )
                }
            )
        ], className="six columns"),

    ],className="row"),
    
    
    html.Br(),
    html.Br(),
    html.Br(),
    
    
    dcc.Dropdown(
        id = "la dropdown list",
        options = [
            {"label" : "Choix 1", "value" : "C1"},
            {"label" : "Choix 2", "value" : "C2"},
            {"label" : "Choix 3", "value" : "C3"}
                ],
        value = "C2",
        multi = True,
        # disabled = True
        ),
    
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    
    html.Label("Le slider qui tue"),
    dcc.Slider(
        min = 1,
        max = 10,
        value = 5,
        marks={i: f"{i}" for i in range(10)}
        # marks = { 0: '0 °F',3: '3 °F',10: '10 °F'}
        ), 


    
    
    dcc.Graph(
        id='scatter one',
        figure={
            'data': [
                go.Scatter(
                    x = Orders.lacolonne1,
                    y = Orders.lacolonne2,
                    mode = "markers"
                    )
            ],
            'layout': go.Layout(
                title = "les sales et les profits",
                xaxis = {"title" : "sales"},
                yaxis = {"title" : "Profit"}
                )
            }
    ),
    
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    
    dcc.Graph(
        id='Nbsouscriptions',
        figure={
            'data': [
                {'x': ["Janvier","Février", "Mars"], 'y': [10, 30, 50], 'type': 'bar', 'name': 'Souscriptions'},
                {'x': ["Janvier","Février", "Mars"], 'y': [10, 20, 30], 'type': 'bar', 'name': 'Objectif de souscription'},
            ],
            'layout': {
                #"plot_bgcolor" : Colors["plot_color"],
                #"paper_bgcolor" : Colors["paper_color"],
                'title': 'Souscriptions à la plateforme DevOps'
                
            }
        }
    ),
    
    html.Br(),
    html.Br(),
    html.Br(),

    
        html.Label("Le input"),
    dcc.Input(
        placeholder = "name",
        type = "text",
        value = "ciou"
        ),
    
    html.Br(),
    html.Br(),
    html.Br(),
    
    dcc.Textarea(
        placeholder = "name",
        style = {"width" : "50%"},
        value = "ciou"
        ),
    
    html.Br(),
    html.Br(),
    html.Br(),

    html.Button("submit mon frère", id = "le bouton de l'acceptation"),

html.Div([
    dcc.Input(id='interactionarea', value='Ici que ca se passe', type='text'),
    html.Div(id='outP div')   
])

])

@app.callback(
    Output(component_id='outP div', component_property='children'),
    [Input(component_id='interactionarea', component_property='value')]
)

def update_tput_div(inpu_value):
    return 'You\'ve  "{}"'.format(inpu_value)

if __name__ == '__main__':
    app.run_server(
        debug=True,
        port=8050,
        host="0.0.0.0"
    )
