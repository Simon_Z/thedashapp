FROM python:3
LABEL maintainer="Simon Zivy, simon.zivy@ibm.com"
RUN apt-get update
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
EXPOSE 8050
ENTRYPOINT [ "python" ]
CMD [ "app.py" ]